use bevy::{prelude::*, input::mouse::MouseWheel};

mod wire_connections;
use wire_connections::*;

pub const NODE_DIAMETER: f32 = 25.0;
pub const GRAVITY: f32 = 9.8;
pub const SPRING_FORCE: f32 = 2.5;
pub const AIR_RESISTANCE: f32 = 2.0;

pub struct WirePlugin;

#[derive(Component)]
pub struct WireNode {
    static_vel: bool,
    velocity: Vec3,
}

pub enum PickedItem {
    StaticNode,
    DynamicNode
}

impl PickedItem {
    fn next(&mut self) {
        match self {
            PickedItem::StaticNode => {
                *self = PickedItem::DynamicNode;
            },
            PickedItem::DynamicNode => {
                *self = PickedItem::StaticNode;
            },
        }
    }

    fn prev(&mut self) {
        match self {
            PickedItem::DynamicNode => {
                *self = PickedItem::StaticNode;
            },
            PickedItem::StaticNode => {
                *self = PickedItem::DynamicNode;
            },
        }
    }
}

struct NodeTextures {
    static_node: Handle<Image>,
    dynamic_node: Handle<Image>,
}

impl Plugin for WirePlugin {
    fn build(&self, app: &mut App) {
        app
            // Node creation
            .add_startup_system(setup_nodes)
            .add_system(spawn_wire_node)
            .add_system(remove_wire_node)
            .add_system(change_item)

            // Node handling
            .add_startup_system(setup_wire_connections)
            .add_system(connect_nodes)
            .add_system(handle_connections.after(handle_physics))
            .add_system_set(SystemSet::on_update(crate::AppState::Running)
                .with_system(handle_physics.after(connect_nodes)));
            
    }
}

// Changes item what we spawn
fn change_item (
    mut wheel_events: EventReader<MouseWheel>,
    mut item: ResMut<PickedItem>,
) {
    for event in wheel_events.iter() {
        match event.unit {
            bevy::input::mouse::MouseScrollUnit::Line => {
                if event.y > 0.0 {
                    item.next();
                } else {
                    item.prev();
                }
            },
            _ => {},
        };
    }
}

// Loads node texture
fn setup_nodes(mut commands: Commands, assets: Res<AssetServer>) {
    commands.insert_resource(PickedItem::StaticNode);

    let texture_static: Handle<Image> = assets.load("node_static.png");
    let texture_dynamic: Handle<Image> = assets.load("node.png");
    commands.insert_resource(NodeTextures { dynamic_node: texture_dynamic, static_node: texture_static });

}

// Spawns node
fn spawn_wire_node(
    mut commands: Commands,
    buttons: Res<Input<MouseButton>>,
    windows: Res<Windows>,
    textures: Res<NodeTextures>,
    item: Res<PickedItem>,
) {
    let window = windows.primary();
    if let Some(cursor_pos) = window.cursor_position() {
        if buttons.just_pressed(MouseButton::Left) {

            let node_is_static: bool;
            let texture: Handle<Image>;

            match *item {
                PickedItem::StaticNode => {
                    node_is_static = true;
                    texture = textures.static_node.as_weak();
                },
                PickedItem::DynamicNode => {
                    node_is_static = false;
                    texture = textures.dynamic_node.as_weak();
                },
            }

            let position =
                cursor_pos - Vec2::new(window.width(), window.height()) / 2.0;

            commands.spawn()
                .insert(WireNode { static_vel: node_is_static, velocity: Vec3::ZERO })
                .insert_bundle(SpriteBundle {
                    transform: Transform {
                        translation: position.extend(0.0),
                        ..default()
                    },
                    sprite: Sprite {
                        custom_size: Some(Vec2::new(NODE_DIAMETER, NODE_DIAMETER)),
                        ..default()
                    },
                    texture: texture,
                    ..default()
                });
        }
    }
}

fn remove_wire_node(
    mut commands: Commands,
    buttons: Res<Input<MouseButton>>,
    keys: Res<Input<KeyCode>>,
    windows: Res<Windows>,
    nodes: Query<(&Transform, Entity), With<WireNode>>,
) {
    if keys.just_pressed(KeyCode::C) {
        for i in nodes.iter() {
            commands.entity(i.1).despawn();
        }
        return;
    }

    let window = windows.primary();
    if let Some(cursor_pos) = window.cursor_position() {
        if buttons.pressed(MouseButton::Right) {
            let position =
                    cursor_pos - Vec2::new(window.width(), window.height()) / 2.0;
            for i in nodes.iter() {
                if i.0.translation.distance(position.extend(0.0)) < NODE_DIAMETER / 2.0 {
                    commands.entity(i.1).despawn();
                }
            }
        }
    }
}
