use bevy::prelude::*;
use super::WireNode;

pub struct PreviousNode(Option<Entity>);

#[derive(Component)]
pub struct Connector(Entity, Entity, Option<f32>);

pub fn setup_wire_connections(mut commands: Commands) {
    commands.insert_resource(PreviousNode(None));
}

// Create connector which renders and handles connection
pub fn connect_nodes (
    mut commands: Commands,
    keys: Res<Input<KeyCode>>,
    windows: Res<Windows>,
    nodes: Query<(&Transform, Entity), With<WireNode>>,
    mut prev_node: ResMut<PreviousNode>,
) {
    if keys.just_pressed(KeyCode::Z) {
        let window = windows.primary();
        if let Some(cursor_pos) = window.cursor_position() {
            let position =
                        cursor_pos - Vec2::new(window.width(), window.height()) / 2.0;
            let mut node: Option<Entity> = None;
            for i in nodes.iter() {
                if i.0.translation.distance(position.extend(0.0)) < super::NODE_DIAMETER / 2.0 {
                    node = Some(i.1);
                    break;
                }
            }

            match node {
                Some(entity) => {
                    match prev_node.0 {
                        Some(prev_entity) => {
                            if prev_entity != entity {
                                // Spawn connectors

                                commands
                                    .spawn()
                                    .insert_bundle(SpriteBundle::default())
                                    .insert(Connector(
                                        entity, prev_entity, None));


                                prev_node.0 = Some(entity);
                            }
                        },
                        None => prev_node.0 = Some(entity),
                    }
                },
                None => {}
            }
        }
    }
}

// Handle connections
pub fn handle_connections (
    mut commands: Commands,
    nodes: Query<(&mut Transform, &mut WireNode)>,
    mut connectors: Query<(Entity, &mut Transform, &mut Connector), Without<WireNode>>,
    state: Res<State<crate::AppState>>,
) {
    for connector_data in connectors.iter_mut() {
        let entity: Entity = connector_data.0;
        let mut connector_transform = connector_data.1;
        let mut connector: Mut<Connector> = connector_data.2;

        unsafe {

            if connector.0 == connector.1 {
                panic!("The connector connects the node to itself!");
            }

            let node_1_transform_result =
                nodes.get_component_unchecked_mut::<Transform>(connector.0);
        
            let node_2_transform_result =
                nodes.get_component_unchecked_mut::<Transform>(connector.1);

            let node_1_result =
                nodes.get_component_unchecked_mut::<super::WireNode>(connector.0);
        
            let node_2_result =
                nodes.get_component_unchecked_mut::<super::WireNode>(connector.1);

            if let (Ok(mut node_1_transform), Ok(mut node_2_transform),
                Ok(mut node_1), Ok(mut node_2)) =
                (node_1_transform_result, node_2_transform_result, node_1_result, node_2_result) {
                    
                // Set the node distance if it is not set
                match connector.2 {
                    None => {
                        connector.2 =
                            Some(node_1_transform.translation.distance(node_2_transform.translation));
                    },
                    _ => {}
                };

                // Resize and move connectors
                let position =
                    (node_1_transform.translation + node_2_transform.translation) / 2.0;

                let vect =
                    node_2_transform.translation - node_1_transform.translation;


                let rotation = 
                    -vect.x.atan2(vect.y);
                    
                let scale =
                    node_1_transform.translation.distance(node_2_transform.translation);

                connector_transform.translation = position; 
                connector_transform.rotation = Quat::from_rotation_z(rotation);
                connector_transform.scale = Vec3::new(1.0, scale, 1.0);
                
                match state.current() {
                    crate::AppState::Running => {
                        // Handle connected nodes
                        if node_1.static_vel == true && node_2.static_vel == true {
                            // We'll do nothing if all of connected nodes are static
                            continue;
                        } else if node_2.static_vel == true {
                            // Make sure node_1 is static instead of node_2
                            (node_2, node_1) = (node_1, node_2);
                            (node_2_transform, node_1_transform) = (node_1_transform, node_2_transform);
                        }

                        if node_1.static_vel == false && node_2.static_vel == false {
                            // If all are dynamic
                            let dist_to_move =
                                (node_1_transform.translation.distance(node_2_transform.translation) -
                                connector.2.unwrap()) / 2.0;

                            if dist_to_move.is_sign_positive() {
                                let node_1_movement =
                                (node_2_transform.translation - node_1_transform.translation)
                                .normalize_or_zero() * dist_to_move;

                                let node_2_movement = -node_1_movement;

                                node_1.velocity += node_1_movement * super::SPRING_FORCE;
                                node_2.velocity += node_2_movement * super::SPRING_FORCE;
                            }

                        } else {
                            // If only one of them is static
                            let dist_to_move =
                                node_1_transform.translation.distance(node_2_transform.translation) -
                                connector.2.unwrap();
                            
                            if dist_to_move.is_sign_positive() {
                                let movement =
                                    (node_1_transform.translation - node_2_transform.translation)
                                    .normalize_or_zero() * dist_to_move;

                                
                                node_2.velocity += movement * super::SPRING_FORCE;
                            }   
                        }
                    },
                    _ => { },
                }
            } else {
                commands.entity(entity).despawn();
            }
        }
    }
}

pub fn handle_physics (
    mut nodes: Query<(&mut Transform, &mut WireNode)>,
    time: Res<Time>,
) {
    for node in nodes.iter_mut() {
        let mut transform: Mut<Transform> = node.0;
        let mut wire_node: Mut<WireNode> = node.1;

        if !wire_node.static_vel {
            wire_node.velocity.y -= super::GRAVITY;
            wire_node.velocity.y = wire_node.velocity.y * (1.0 - super::AIR_RESISTANCE * time.delta_seconds());
        }

        transform.translation += wire_node.velocity * time.delta_seconds();
    }
}
