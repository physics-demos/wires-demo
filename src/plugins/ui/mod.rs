use bevy::prelude::*;
pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_ui);
        app.add_system(update_ui);
    }
}

#[derive(Component)]
struct PickedItemImage;

struct UiTextures {
    static_node: Handle<Image>,
    dynamic_node: Handle<Image>,
}

fn setup_ui(mut commands: Commands, assets: Res<AssetServer>) {
    // Load textures

    let static_node = assets.load("node_static.png");
    let dynamic_node = assets.load("node.png");
    let static_node_weak = static_node.as_weak();

    commands.insert_resource(UiTextures {
        static_node,
        dynamic_node
    });

    // Background
    commands.spawn_bundle(NodeBundle {
        style: Style {
            size: Size::new(Val::Px(50.0), Val::Px(50.0)),
            position_type: PositionType::Absolute,
            position: Rect {
                right: Val::Px(10.0),
                top: Val::Px(10.0),
                ..default()
            },
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.75).into(),
        ..default()
    }).with_children(|parent| {
        // Image of action we are going to perform
        parent.spawn_bundle(ImageBundle {
            style: Style { 
                size: Size::new(Val::Percent(90.0), Val::Percent(90.0)),
                margin: Rect::all(Val::Auto),
                ..default()
            },
            image: static_node_weak.into(),
            ..default()
        }).insert(PickedItemImage);
    });
}

fn update_ui(
    mut images: Query<&mut UiImage, With<PickedItemImage>>,
    item: Res<super::wire_nodes::PickedItem>,
    textures: Res<UiTextures>,
) {
    let texture: &Handle<Image>;
    match *item {
        super::wire_nodes::PickedItem::StaticNode => {
            texture = &textures.static_node;
        },
        super::wire_nodes::PickedItem::DynamicNode => {
            texture = &textures.dynamic_node;
        }
    }
    images.for_each_mut(|mut image| {
        image.0 = texture.as_weak();
    });
}