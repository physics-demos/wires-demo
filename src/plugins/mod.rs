use bevy::prelude::*;

mod wire_nodes;
mod ui;
use wire_nodes::WirePlugin;
use ui::UiPlugin;

pub struct SimulationPlugins;

impl PluginGroup for SimulationPlugins {
    fn build(&mut self, group: &mut bevy::app::PluginGroupBuilder) {
        group.add(WirePlugin)
            .add(UiPlugin);
    }
}