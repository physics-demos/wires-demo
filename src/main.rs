use bevy::prelude::*;

mod plugins;
use plugins::*;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum AppState {
    Running,
    Paused
}

fn main() {
    App::new()
        .insert_resource(Msaa::default())
        .insert_resource(ClearColor::default())
        .add_state(AppState::Running)
        .add_plugins(DefaultPlugins)
        .add_plugins(SimulationPlugins)
        .add_startup_system(setup)
        .add_system(pause)
        .run();
}

fn pause(mut state: ResMut<State<AppState>>, mut color: ResMut<ClearColor>, keys: Res<Input<KeyCode>> ) {
    if keys.just_pressed(KeyCode::Space) {
        match state.current() {
            AppState::Paused => {
                state.set(AppState::Running).unwrap();
                *color = ClearColor::default();
            },
            AppState::Running => {
                state.set(AppState::Paused).unwrap();
                *color = ClearColor(Color::DARK_GRAY);
            },
        }

    }
}

fn setup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());
}
